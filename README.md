# Výsledky testovania SK INSPIRE zdrojov

Projekt obsahujúci výsledky testovania INSPIRE priestorových údajov, služieb a ich metaúdajov poskytovaných Povinnými osobami a dobrovoľnými prispievateĺmi do Národnej infraštruktúry priestorových informácií (http://inspire.gov.sk). 

Testovanie je realizované prostredníctvom INSPIRE referenčného validátora (http://inspire-sandbox.jrc.ec.europa.eu/validator/), konkrétne jeho Aplikačno programového rozhrania - API (http://inspire-sandbox.jrc.ec.europa.eu/etf-webapp/swagger-ui.html).

Zoznam  testovaných INSPIRE SK zdrojov:
https://docs.google.com/spreadsheets/d/1h2ATgroUORQS_5uoH2v1sCT5EAGhPOHMp4mAzAkapbE/edit?usp=sharing

Zoznam  identifikovaných problémov so stavom ich riešenia:
https://gitlab.com/mzpsr/mzpsr/sk-inspire-testing/vysledky-testovania-sk-inspire-zdrojov/issues